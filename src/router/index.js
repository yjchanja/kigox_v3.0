import { createRouter, createWebHistory } from 'vue-router'

const MainNav = () => import(/* webpackChunkName: "mainNav" */ '@/components/nav/MainNav')
const BackNav = () => import(/* webpackChunkName: "backNav" */ '@/components/nav/BackNav')
const Footer = () => import(/* webpackChunkName: "footer" */ '@/components/nav/Footer')
const Login = () => import(/* webpackChunkName: "login" */ '@/views/Login')
const Home = () => import(/* webpackChunkName: "home" */ '@/views/Home')
const Detail = () => import(/* webpackChunkName: "detail" */ '@/views/Detail');

const routes = [
  {
    path: '/',
    name: 'login',
    components: {default : Login}
  },
  {
    path: '/home',
    name: 'home',
    components: {nav : MainNav, default : Home, footer : Footer}
  },
  {
    path: '/detail',
    name: 'detail',
    components: {nav : BackNav, default : Detail, footer : Footer}
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
